/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.controller;

import application.entity.Po;
import application.entity.PoDetail;
import constructionsolution.NumberGenerator;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Shashiprabha
 */
public class PoController extends DbCommon{
    
    public boolean savePoDetais(Po po) {

        Session session = super.getHibernateUtil().openWritebleDbConnection();
        List<PoDetail> list = null;
        try {

            session.save(po);

       String genaretedRentalNo = new NumberGenerator().getGenaretedNumber("REN", po.getPoId());
            po.setPoNo(genaretedRentalNo);
            session.saveOrUpdate(po);

            list = po.getPoDetails();
            Iterator<PoDetail> itr = list.iterator();
            while (itr.hasNext()) {
                PoDetail detail = itr.next();
                detail.setPodetailsPoRef(po);
                session.saveOrUpdate(detail);
            }

            session.getTransaction().commit();
        } catch (Exception ex) {
            session.getTransaction().rollback();
            ex.printStackTrace();
            return false;
        } finally {
            if (list != null) {
                list.clear();
            }
            session.close();
        }
        return true;
    }

    
}
